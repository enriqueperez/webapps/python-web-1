FROM centos:latest
MAINTAINER Enrique perez
USER root

ENV APP_LOG_FILE '/var/www/html/log/app.log'
#ENV APP_USERNAME 'username'
#ENV APP_PASSWORD 'password'

RUN yum -y update && yum -y upgrade
RUN yum -y install epel-release \
    && yum install -y httpd python36 python36-devel python3-pip \
    && yum install -y gcc gd gd-devel

WORKDIR /usr/bin/
RUN pip3.6 install --upgrade pip
RUN pip3.6 install requests

RUN mkdir /var/www/html/bin
RUN mkdir /var/www/html/log

COPY conf/myweb.conf /etc/httpd/conf.d/
COPY app/* /var/www/html/bin/

WORKDIR /var/www/html/
RUN ln -s /var/www/html/bin/index.html index.html
RUN chown -R apache.apache /var/www/html
RUN chmod 700 /var/www/html/bin/myweb.py

RUN touch $APP_LOG_FILE
RUN chown apache.apache $APP_LOG_FILE
VOLUME /var/www/html/log

EXPOSE 80

ENTRYPOINT ["/usr/sbin/httpd"]
CMD ["-D", "FOREGROUND"]
