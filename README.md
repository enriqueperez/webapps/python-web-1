# python-web-1
# Revision v1

For training purposes. Project using HTML5+CSS3, Docker and Python3.6

# Branches
- master (Release)
- v1 (Build and test)

# Stages:
- Build Image
- Test image
- Release
